<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Data Yang Belum Melakukan KGB') }}
        </h2>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.6/jspdf.plugin.autotable.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="grid grid-cols-2 gap-6">
                        <div class="grid grid-rows-1 gap-6">
                            <form action="{{ route('mains.index') }}" method="GET" role="search" autocomplete="off">
                                <div class="input-group">
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                            Bulan, Tahun :
                                        </span>
                                        <input id="last_kgb" class="date block focus:ring-indigo-500 focus:border-indigo-500 flex-1 rounded-none rounded-r-md sm:text-sm border-gray-300" type="text" name="last_kgb">
                                        <script type="text/javascript">
                                            $('.date').datepicker({
                                                format: "yyyy-mm",
                                                startView: "months",
                                                minViewMode: "months",
                                                // todayHighlight: true,
                                                autoclose: true,
                                            });
                                        </script>
                                    </div>
                                    <x-button type="submit" class="ml-1 mt-1">
                                        Cari
                                    </x-button>
                                </div>
                            </form>
                        </div>
                        <div class="grid grid-rows-1 gap-6 ">
                            <a>
                                <x-button onclick="generate()" type="button" class="ml-1 mt-1 float-right text-white bg-indigo-600 hover:bg-indigo-700">
                                    Export to PDF
                                </x-button>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="flex flex-col">
                        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                    <table id="simple_table" class="min-w-full divide-y divide-gray-200">
                                        <thead class="bg-gray-50">
                                            <tr>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    NIP
                                                </th>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Name
                                                </th>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Reminder
                                                </th>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    KGB Berikutnya
                                                </th>
                                                <th scope="col" class="relative px-6 py-3">
                                                    <span class="sr-only">Action KGB</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-200">
                                            @foreach ($kgbs as $kgb)
                                            <tr>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {{ $kgb->nip }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {{ strtoupper($kgb->nama) }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                        {{ $kgb->reminder }}
                                                    </span>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {{ $kgb->next_kgb }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                    <a href="{{ route('mains.edit', $kgb) }}" class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
                                                        Validasi
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mt-4">
                                    {{ $kgbs->links() }}
                                </div>
                                <script type="text/javascript">
                                    function generate() {
                                        var doc = new jsPDF('p', 'pt', 'letter');
                                        var htmlstring = '';
                                        var tempVarToCheckPageHeight = 0;
                                        var pageHeight = 0;
                                        pageHeight = doc.internal.pageSize.height;
                                        specialElementHandlers = {
                                            // element with id of "bypass" - jQuery style selector  
                                            '#bypassme': function(element, renderer) {
                                                // true = "handled elsewhere, bypass text extraction"  
                                                return true
                                            }
                                        };
                                        margins = {
                                            top: 150,
                                            bottom: 60,
                                            left: 40,
                                            right: 40,
                                            width: 600
                                        };
                                        var today = new Date();
                                        // var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                                        var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
                                        var y = 20;
                                        doc.setLineWidth(2);
                                        doc.text(310, 35, "Data Yang belum Validasi Berkas KGB \n " + date + '\n', {
                                            align: 'center'
                                        });
                                        doc.setfon
                                        doc.autoTable({
                                            html: '#simple_table',
                                            startY: 70,
                                            theme: 'grid',
                                            // columnStyles: {
                                            //     0: {
                                            //         cellWidth: 180,
                                            //     },
                                            //     1: {
                                            //         cellWidth: 180,
                                            //     },
                                            //     2: {
                                            //         cellWidth: 180,
                                            //     }
                                            // },
                                            // styles: {
                                            //     minCellHeight: 40
                                            // }
                                        })
                                        doc.save('KGB ' + date + '.pdf');
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>