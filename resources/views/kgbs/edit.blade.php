<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Update Data') }}
        </h2>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('kgbs.update', $kgb->id) }}" autocomplete="off">
                @method('PUT')
                @csrf
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200">
                        <!-- NIP -->
                        <div>
                            <x-label for="nip" :value="__('NIP')" />
                            <x-input id="nip" class="block mt-1 w-full" type="number" name="nip" value="{{ $kgb->nip }}" required autofocus />
                        </div>

                        <!-- Nama -->
                        <div class="mt-4">
                            <x-label for="nama" :value="__('Nama')" />
                            <x-input id="nama" class="block mt-1 w-full" type="text" name="nama" value="{{ $kgb->nama }}" required />
                        </div>

                        <!-- Unit Kerja -->
                        <div class="mt-4">
                            <x-label for="unit_kerja" :value="__('Unit Kerja')" />
                            <x-input id="unit_kerja" class="block mt-1 w-full" type="text" name="unit_kerja" value="{{ $kgb->unit_kerja }}" required />
                        </div>

                        <!-- Golongan -->
                        <div class="mt-4">
                            <x-label for="golongan" :value="__('Golongan')" />
                            <x-input id="golongan" class="block mt-1 w-full" type="text" name="golongan" value="{{ $kgb->golongan }}" required />
                        </div>

                        <div class="mt-4">
                            <x-label for="last_kgb" :value="__('KGB Terakhir')" />
                            <x-input id="last_kgb" class="date block mt-1 w-full" type="text" name="last_kgb" value="{{ $kgb->last_kgb }}" required />
                            <script type="text/javascript">
                                $('.date').datepicker({
                                    format: 'yyyy-mm-dd'
                                });
                            </script>
                        </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <div class="flex items-center justify-end">
                            <x-button class="ml-3" type="submit">
                                Update
                            </x-button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>