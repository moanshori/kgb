<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    
    Route::view('dashboard', 'dashboard')->name('dashboard');

    Route::resource('kgbs', \App\Http\Controllers\KgbController::class);
    // Route::resource('histories', \App\Http\Controllers\KgbController::class);
    // Route::resource('mains', \App\Http\Controllers\MainController::class);
    Route::resource('mains', \App\Http\Controllers\MainController::class)->parameters(['mains' => 'kgb']);
});

require __DIR__.'/auth.php';
