<?php

namespace App\Http\Controllers;

use DateTime, DateInterval;
use App\Models\Kgb;
use \Carbon\Carbon;
use Illuminate\Http\Request;

class KgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // $kgbs = kgb::paginate();
        $a = "a";
        $kgbs = kgb::where([
            ['nama', '!=', NULL],
            [function ($query) use ($request) {
                if (($term = $request->term)) {
                    $query->orWhere('nama', 'LIKE', '%' . $term . '%')->get();
                }
            }]
        ])->orderBy("id", "asc")->paginate(5);

        return view('kgbs.index', compact('kgbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kgbs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        // error_log(print_r($input, true));

        $tgl = substr($input['nip'], 6, 2);
        $bln = substr($input['nip'], 4, 2);
        $thn = substr($input['nip'], 0, 4);

        $pensiun = ((int)$thn + 60) . "-" . $bln . "-" . $tgl;
        // error_log($pensiun);

        $tgl = substr($input['last_kgb'], 8, 2);
        $bln = substr($input['last_kgb'], 5, 2);
        $thn = substr($input['last_kgb'], 0, 4);

        $next_kgb = ((int)$thn + 2) . "-" . $bln . "-" . $tgl;

        // error_log($next_kgb);
        $currentDate = new DateTime($next_kgb);
        $yesterdayDT = $currentDate->sub(new DateInterval('P2M'));

        //Get the date in a YYYY-MM-DD format.
        $yesterday = $yesterdayDT->format('Y-m-d');
        // $reminder = ((int)$thn+2)."-".$bln."-".$tgl;


        // $collection = array('pensiun' => $pensiun, 'reminder' => $yesterday, 'status' => 0);
        $collection = array(
            'pensiun' => $pensiun, 'reminder' => $yesterday, 'status' => 0, 'next_kgb' => $next_kgb,
            'p_kgb' => 1, 'n_kgb' => 0
        );
        // $collection['status'] = 0;
        // error_log(print_r($collection, true));

        error_log(print_r(array_merge($input, $collection), true));

        $data = array_merge($input, $collection);
        kgb::create($data);
        // kgb::create($input);

        return redirect()->route('kgbs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kgb  $kgb
     * @return \Illuminate\Http\Response
     */
    public function show(Kgb $kgb)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kgb  $kgb
     * @return \Illuminate\Http\Response
     */
    public function edit(Kgb $kgb)
    {
        //
        // error_log(print_r($kgb, true));

        return view('kgbs.edit', compact('kgb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kgb  $kgb
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kgb $kgb)
    {
        //
        $input = $request->all();

        $tgl = substr($input['nip'], 6, 2);
        $bln = substr($input['nip'], 4, 2);
        $thn = substr($input['nip'], 0, 4);
        $pensiun = ((int)$thn + 60) . "-" . $bln . "-" . $tgl;

        $tgl = substr($input['last_kgb'], 8, 2);
        $bln = substr($input['last_kgb'], 5, 2);
        $thn = substr($input['last_kgb'], 0, 4);

        $next_kgb = ((int)$thn + 2) . "-" . $bln . "-" . $tgl;

        $currentDate = new DateTime($next_kgb);
        $yesterdayDT = $currentDate->sub(new DateInterval('P2M'));
        $yesterday = $yesterdayDT->format('Y-m-d');

        $collection = array(
            'pensiun' => $pensiun, 'reminder' => $yesterday, 'status' => 0, 'next_kgb' => $next_kgb,
            'p_kgb' => 1, 'n_kgb' => 0
        );

        // error_log(print_r(array_merge($input, $collection), true));

        $data = array_merge($input, $collection);

        error_log(print_r($input, true));
        error_log(print_r($data, true));

        $kgb->update($data);

        return redirect()->route('kgbs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kgb  $kgb
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kgb $kgb)
    {
        //
        // error_log(print_r($kgb->all(), true));

        $kgb->delete();
        return redirect()->route('kgbs.index');
    }
}
