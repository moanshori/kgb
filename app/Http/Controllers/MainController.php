<?php

namespace App\Http\Controllers;

use App\Models\Kgb;
use App\Models\Main;
use DateTime, DateInterval;
// use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // $mains = DB::table('kgbs')->where('nama', 'Golobok')->get();
        $time = \Carbon\Carbon::now()->format('Y-m');
        error_log($time);

        if (sizeof($request->all()) == 0) {
            $kgbs = kgb::where([
                ['reminder', '!=', NULL],
                [function ($query) use ($request) {
                    if ($time = \Carbon\Carbon::now()->format('Y-m')) {
                        // $time = \Carbon\Carbon::now()->format('Y-m');
                        $time = $query->orWhere('reminder', 'LIKE', $time . '-%%')->get();
                    }
                }]
            ])->orderBy("nama", "asc")->paginate(5);
            error_log("0");
        } else {
            error_log("1");
            $kgbs = kgb::where([
                ['reminder', '!=', NULL],
                [function ($query) use ($request) {
                    if ($last_kgb = $request->last_kgb) {
                        $last_kgb = $query->orWhere('reminder', 'LIKE', $last_kgb . '-%%')->get();
                    }
                }]
            ])->orderBy("nama", "asc")->paginate(5);
        }
        return view('mains.index', compact('kgbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function show(Kgb $kgb)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function edit(Kgb $kgb)
    {
        //
        // error_log(print_r($kgb, true));
        return view('mains.edit', compact('kgb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // error_log("baris eror");
        // $id = $request->input();
        // error_log(print_r($request->all(), true));
        // error_log(print_r($id, true));

        $kgb = Kgb::find($id);
        // error_log(print_r($kgb, true));
        // error_log(print_r($kgb['nama'], true));

        // $input = $request->all();
        // error_log(print_r($input, true));

        $tgl = substr($kgb['last_kgb'], 8, 2);
        $bln = substr($kgb['last_kgb'], 5, 2);
        $thn = substr($kgb['last_kgb'], 0, 4);
        $last_kgb = ((int)$thn + 2) . "-" . $bln . "-" . $tgl;

        $tgl = substr($kgb['next_kgb'], 8, 2);
        $bln = substr($kgb['next_kgb'], 5, 2);
        $thn = substr($kgb['next_kgb'], 0, 4);
        $next_kgb = ((int)$thn + 2) . "-" . $bln . "-" . $tgl;

        $tgl = substr($kgb['reminder'], 8, 2);
        $bln = substr($kgb['reminder'], 5, 2);
        $thn = substr($kgb['reminder'], 0, 4);
        $reminder = ((int)$thn + 2) . "-" . $bln . "-" . $tgl;

        $collection = array(
            'next_kgb' => $next_kgb, 'reminder' => $reminder, 'last_kgb' => $last_kgb
        );

        $data = array_merge($request->all(), $collection);
        error_log(print_r($data, true));

        $kgb->update($data);

        return redirect()->route('mains.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kgb $kgb)
    {
        //
        // error_log(print_r($kgb->all(), true));

        // $kgb->delete();
        return redirect()->route('mains.index');
    }
}
