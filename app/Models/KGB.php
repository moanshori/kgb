<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kgb extends Model
{
    use HasFactory;

    protected $fillable = [
        'nip',
        'nama',
        'unit_kerja',
        'last_kgb',
        'next_kgb',
        'golongan',
        'pensiun',
        'reminder',
        'status',
        'p_kgb',
        'n_kgb'
    ];
}
