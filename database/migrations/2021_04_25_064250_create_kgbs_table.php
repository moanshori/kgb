<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKgbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kgbs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip'); //nip
            $table->string('nama');
            $table->string('unit_kerja');
            $table->string('golongan');
            $table->date('last_kgb'); // KGB terakhir
            $table->date('next_kgb'); // KGB berikutnya
            $table->date('reminder'); // pengingat kbg berikutnya H-2 bulan
            $table->date('pensiun'); // tgl bln th pensiun
            $table->integer('status'); // status pensiun
            $table->integer('p_kgb'); // prev kgb
            $table->integer('n_kgb'); // next kgb
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kgbs');
    }
}
